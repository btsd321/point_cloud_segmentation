#ifndef _PIPESSETCLOUD_H_
#define _PIPESSETCLOUD_H_

//#include "CloudOperation.h"
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>
#include "EndFaceCloud.h"

using namespace pcl;

//定义该变量则会进行坐标转换
//constexpr float UNIT_CLOUD = 100.0f;//点云坐标单位，即1个数据单位在实际为多少mm
//主方向
constexpr pcl::Normal agreed_normals_param = {0,0,1};


typedef enum ENUM_CLOUD_TYPE {
	EM_CLOUD_TYPE_ORIGINAL = 0,
	EM_CLOUD_TYPE_DOWN_SAMPLED,
	EM_CLOUD_TYPE_PASS_THROUGH_FILTERED,
	EM_CLOUD_TYPE_STATISTICAL_OUTLIER_FILTERED,
	EM_CLOUD_TYPE_DIR_FILLED,
	EM_CLOUD_TYPE_MAX,
}EM_CLOUD_TYPE;

class PipesSetCloud : public PointCloud<PointXYZ>
{
public:

	using Ptr = std::shared_ptr<PipesSetCloud>;
	using ConstPtr = std::shared_ptr<const PipesSetCloud>;

	PipesSetCloud(const PointCloud<PointXYZ> &cloud);
	PipesSetCloud(const PointCloud<PointXYZ>::Ptr& pcloud);
	virtual ~PipesSetCloud();

	//void DownSample(const DOWN_SAMPLE_IN& in, DOWN_SAMPLE_OUT& out);
	PointCloud<PointXYZ>& PassThroughFilter(float x_min, float x_max, float y_min, float y_max, float z_min, float z_max);
	PointCloud<PointXYZ>& StatisticalOutlierRemoval(int mean_k, double std_dev_mul_thresh);
	PointCloud<PointXYZ>& DownSample2(double fRadiusSearch);
	PointCloud<PointXYZ>& DownSample1(double fRadiusSearch);
	void CloudSegmentation(double tolerance);
	
	PointCloud<PointXYZ>& DirFiler(int KSearch1, double fRadiusSearch1, int KSearch2, int mean_k2, double std_dev_mul_thresh2, float fRadiusSearch2);

	PointCloud<PointXYZ>& getOriginalCloud();
	PointCloud<PointXYZ>& getDownSampledCloud1();
	PointCloud<PointXYZ>& getDownSampledCloud2();
	PointCloud<PointXYZ>& getPassThroughFilteredCloud();
	PointCloud<PointXYZ>& getStatisticalOutlierFilteredCloud();

	void DisplayCloud(EM_CLOUD_TYPE emCloudType);

	std::list<EndFaceCloud*> pEndFaceClouds;
};


#endif