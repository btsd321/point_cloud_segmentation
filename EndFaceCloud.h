#ifndef _ENDFACECLOUD_H_
#define _ENDFACECLOUD_H_

//#include "CloudOperation.h"
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>

using namespace pcl;

class EndFaceCloud;
class TorusEdgeCloud;

class EndFaceCloud : public PointCloud<PointXYZ>
{
public:

	using Ptr = std::shared_ptr<EndFaceCloud>;
	using ConstPtr = std::shared_ptr<const EndFaceCloud>;

	EndFaceCloud();
	EndFaceCloud(const PointCloud<PointXYZ>::Ptr& cloud);
	EndFaceCloud(const PointCloud<PointXYZ>& cloud);
	EndFaceCloud(const EndFaceCloud& other);
	virtual ~EndFaceCloud() {};

	int EdgeExtraction(TorusEdgeCloud& edgeCloud) const;//边缘提取
	void Display();

	//pcl::PointCloud<pcl::Normal> normal_cloud;
};

class TorusEdgeCloud : public EndFaceCloud
{
public:

	using Ptr = std::shared_ptr<TorusEdgeCloud>;
	using ConstPtr = std::shared_ptr<const TorusEdgeCloud>;

	TorusEdgeCloud();
	TorusEdgeCloud(const PointCloud<PointXYZ>::Ptr& cloud);
	TorusEdgeCloud(const PointCloud<PointXYZ>& cloud);
	//TorusEdgeCloud(const EndFaceCloud& cloud);
	TorusEdgeCloud(const TorusEdgeCloud& other);
	virtual ~TorusEdgeCloud() {};

	int TorusFit();//圆环拟合
	bool IsNormalShape() const;//判断形状是否正常
	void Display();

	double getCenterX() const { return center_x; }
	double getCenterY() const { return center_y; }
	double getCenterZ() const { return center_z; }
	//double getRadiusA() const { return radius_a; }
	//double getRadiusB() const { return radius_b; }
	double getAngleA() const { return angle_a; }
	double getAngleB() const { return angle_b; }
	double getAngleC() const { return angle_c; }

private:
	double center_x, center_y, center_z;//圆心坐标
	//double radius_a, radius_b;//内径a, 外径b
	double angle_a, angle_b, angle_c;//圆环角度

	pcl::ModelCoefficients coefficients;

};


#endif

