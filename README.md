# point_cloud_segmentation

#### 介绍
多柱形捆扎后端面点云分割提取并计算端面圆心等参数

#### 软件架构
软件架构说明


#### 安装教程
运行环境windows、Cmake
需要PCL、HalconCpp库
1.  需要安装PCL库，下载地址：https://github.com/PointCloudLibrary/pcl，下载PCL-1.14.1-AllInOne-msvc2022-win64.exe
    windows下安装PCL库时需要注意会无法正常添加PCL系统环境变量，需要手动添加才能正常运行程序，手动添加环境变量的具体操作见https://blog.csdn.net/zhangxz259/article/details/85683201

2.  需要安装HalconCpp库，本程序使用的版本为Halcon-2024，正常安装后Halcon会自动添加系统环境变量，如果没有添加，需要手动添加
3.  需要安装Cmake，下载地址：https://cmake.org/download/，下载cmake-3.30.2-windows-x86_64.msi，安装后需要添加cmake的bin目录到系统环境变量PATH中

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
