#ifndef _CLOUDOPERATION_H_
#define _CLOUDOPERATION_H_

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/ModelCoefficients.h>

using namespace pcl;

class CloudOperation {
public:
    CloudOperation() {};
    ~CloudOperation() {};
    void CloudOperation::CreateCircle3DCloud(const ModelCoefficients& coefficients, PointCloud<PointXYZ>& OutputCloud);
};


#endif
